resource "aws_cloudwatch_metric_alarm" "auto_recovery_alarm" {
  count               = var.recovery_enabled == true ? 1 : 0
  alarm_name          = "EC2AutoRecover-${module.ec2.id[0]}"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "3"
  metric_name         = "StatusCheckFailed_System"
  namespace           = "AWS/EC2"
  period              = "60"
  statistic           = "Minimum"

  dimensions = {
    InstanceId = module.ec2.id[0]
  }

  alarm_actions = ["arn:aws:automate:${var.region}:ec2:recover"]

  threshold         = "1"
  alarm_description = "Auto recover the EC2 instance if Status Check fails."
}
