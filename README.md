# EC2 MODULE

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:-----:|
| ami | ID of AMI to use for the instance | `string` | n/a | yes |
| associate\_public\_ip\_address | If true, the EC2 instance will have associated public IP address | `bool` | `true` | no |
| ebs\_volumes | list of ebs volumes | <pre>list(object({<br>    device_name           = string<br>    volume_size           = number<br>    volume_type           = string<br>    delete_on_termination = bool<br>  }))</pre> | n/a | yes |
| ingress\_ports | A map of ports which will be enabled in ingress sg rules | <pre>list(object({<br>    port             = number<br>    protocol         = string<br>    cidr_blocks      = list(string)<br>    ipv6_cidr_blocks = list(string)<br>  }))</pre> | <pre>[<br>  {<br>    "cidr_blocks": [<br>      "0.0.0.0/0"<br>    ],<br>    "ipv6_cidr_blocks": [<br>      "::/0"<br>    ],<br>    "port": 22,<br>    "protocol": "tcp"<br>  }<br>]</pre> | no |
| instance\_count | Number of instances to launch | `number` | `1` | no |
| instance\_type | The type of instance to start | `string` | n/a | yes |
| instance\_volume\_size\_gb | The root volume size, in gigabytes | `number` | `20` | no |
| ipv6\_address\_count | A number of IPv6 addresses to associate with the primary network interface. Amazon EC2 chooses the IPv6 addresses from the range of your subnet. | `number` | `1` | no |
| ipv6\_enable | If true, the EC2 instance will get ipv6 adress | `bool` | `true` | no |
| key\_name | The key name to use for the instance | `string` | `""` | no |
| monitoring | If true, the launched EC2 instance will have detailed monitoring enabled | `bool` | `false` | no |
| name | Name to be used on all resources as prefix | `string` | n/a | yes |
| recovery\_enabled | When enabled ec2 instance when failed will be rebooted | `bool` | `true` | no |
| region | AWS region to deploy the ec2 in. It is used for AWS CLI calls from within ec2 host (userdata) | `string` | n/a | yes |
| root\_volume\_termination | If True then root device volume will be destroy | `bool` | `true` | no |
| route53\_cname\_private\_subdomain | Full domain address of the CNAME that will point to the ec2 host EC2 public domain | `list(string)` | n/a | yes |
| route53\_cname\_subdomain | Full domain address of the CNAME that will point to the ec2 host EC2 public domain | `list(string)` | n/a | yes |
| route53\_domain | Domain name to use in Route53 to add a record for ec2 host from within its own userdata | `string` | n/a | yes |
| route53\_zone\_id | ID of the hosted zone ec2 CNAME is to be created under | `string` | n/a | yes |
| subnet\_id | The VPC Subnet ID to launch in | `string` | `""` | no |
| subnet\_ids | A list of VPC Subnet IDs to launch in | `list(string)` | `[]` | no |
| tags | A mapping of tags to assign to the resource | `map(string)` | `{}` | no |
| user\_data | The user data to provide when launching the instance. | `string` | n/a | yes |
| vpc\_id | ID of the VPC to deploy into | `string` | n/a | yes |
| vpc\_security\_group\_ids | A list of security group IDs to associate with | `list(string)` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| arn | List of ARNs of instances |
| availability\_zone | List of availability zones of instances |
| dns\_records\_private | n/a |
| dns\_records\_public | n/a |
| id | List of IDs of instances |
| ipv6\_addresses | List of assigned IPv6 addresses of instances |
| key\_name | List of key names of instances |
| private\_dns | List of private DNS names assigned to the instances. Can only be used inside the Amazon ec2, and only available if you've enabled DNS hostnames for your VPC |
| private\_ip | List of private IP addresses assigned to the instances |
| public\_dns | List of public DNS names assigned to the instances. For ec2-VPC, this is only available if you've enabled DNS hostnames for your VPC |
| public\_ip | List of public IP addresses assigned to the instances, if applicable |
| security\_groups | List of associated security groups of instances |
| subnet\_id | List of IDs of VPC subnets of instances |
| tags | List of tags of instances |
| vpc\_security\_group\_ids | List of associated security groups of instances, if running in non-default VPC |
