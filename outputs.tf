output "id" {
  description = "List of IDs of instances"
  value       = module.ec2.*.id
}

output "arn" {
  description = "List of ARNs of instances"
  value       = module.ec2.*.arn
}

output "availability_zone" {
  description = "List of availability zones of instances"
  value       = module.ec2.*.availability_zone
}

output "key_name" {
  description = "List of key names of instances"
  value       = module.ec2.*.key_name
}

output "public_dns" {
  description = "List of public DNS names assigned to the instances. For ec2-VPC, this is only available if you've enabled DNS hostnames for your VPC"
  value       = module.ec2.*.public_dns
}

output "public_ip" {
  description = "List of public IP addresses assigned to the instances, if applicable"
  value       = module.ec2.*.public_ip
}

output "ipv6_addresses" {
  description = "List of assigned IPv6 addresses of instances"
  value       = module.ec2.*.ipv6_addresses
}

output "private_dns" {
  description = "List of private DNS names assigned to the instances. Can only be used inside the Amazon ec2, and only available if you've enabled DNS hostnames for your VPC"
  value       = module.ec2.*.private_dns
}

output "private_ip" {
  description = "List of private IP addresses assigned to the instances"
  value       = module.ec2.*.private_ip
}

output "security_groups" {
  description = "List of associated security groups of instances"
  value       = module.ec2.*.security_groups
}

output "vpc_security_group_ids" {
  description = "List of associated security groups of instances, if running in non-default VPC"
  value       = module.ec2.*.vpc_security_group_ids
}

output "subnet_id" {
  description = "List of IDs of VPC subnets of instances"
  value       = module.ec2.*.subnet_id
}

output "tags" {
  description = "List of tags of instances"
  value       = module.ec2.*.tags
}

output "dns_records_private" {
  value = aws_route53_record.ec2-records-private.*.name
}

output "dns_records_public" {
  value = aws_route53_record.ec2-records-public.*.name
}

