resource "aws_security_group" "sg-ec2" {
  name        = var.name
  vpc_id      = var.vpc_id
  description = "ec2 security group (only SSH inbound access is allowed)"

  dynamic "ingress" {
    for_each = var.ingress_ports
    iterator = rule
    content {
      from_port        = rule.value["port"]
      to_port          = rule.value["port"]
      protocol         = rule.value["protocol"]
      cidr_blocks      = rule.value["cidr_blocks"]
      ipv6_cidr_blocks = rule.value["ipv6_cidr_blocks"]
    }
  }

  tags = {
    Name = "sg-${var.name}"
  }
}

resource "aws_security_group_rule" "ec2_all_egress" {
  type      = "egress"
  from_port = "0"
  to_port   = "65535"
  protocol  = "all"

  cidr_blocks = [
    "0.0.0.0/0",
  ]

  ipv6_cidr_blocks = [
    "::/0",
  ]

  security_group_id = aws_security_group.sg-ec2.id
}
