variable "name" {
  description = "Name to be used on all resources as prefix"
  type        = string
}

variable "instance_count" {
  description = "Number of instances to launch"
  type        = number
  default     = 1
}

variable "ami" {
  description = "ID of AMI to use for the instance"
  type        = string
}

variable "instance_type" {
  description = "The type of instance to start"
  type        = string
}

variable "key_name" {
  description = "The key name to use for the instance"
  type        = string
  default     = ""
}

variable "user_data" {
  description = "The user data to provide when launching the instance."
  type        = string
  default     = null
}

variable "monitoring" {
  description = "If true, the launched EC2 instance will have detailed monitoring enabled"
  type        = bool
  default     = false
}

variable "vpc_security_group_ids" {
  description = "A list of security group IDs to associate with"
  type        = list(string)
  default     = null
}

variable "subnet_id" {
  description = "The VPC Subnet ID to launch in"
  type        = string
  default     = ""
}

variable "subnet_ids" {
  description = "A list of VPC Subnet IDs to launch in"
  type        = list(string)
  default     = []
}

variable "associate_public_ip_address" {
  description = "If true, the EC2 instance will have associated public IP address"
  type        = bool
  default     = true
}

variable "ipv6_address_count" {
  description = "A number of IPv6 addresses to associate with the primary network interface. Amazon EC2 chooses the IPv6 addresses from the range of your subnet."
  type        = number
  default     = 1
}

variable "ipv6_enable" {
  description = "If true, the EC2 instance will get ipv6 adress"
  type        = bool
  default     = true
}

variable "instance_volume_size_gb" {
  description = "The root volume size, in gigabytes"
  type        = number
  default     = 20
}

variable "tags" {
  description = "A mapping of tags to assign to the resource"
  type        = map(string)
  default     = {}
}

variable "vpc_id" {
  description = "ID of the VPC to deploy into"
  type        = string
}

variable "ingress_ports" {
  description = "A map of ports which will be enabled in ingress sg rules"
  type = list(object({
    port             = number
    protocol         = string
    cidr_blocks      = list(string)
    ipv6_cidr_blocks = list(string)
  }))
  default = [{
    port             = 22
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }]
}

variable "route53_zone_id" {
  description = "ID of the hosted zone ec2 CNAME is to be created under"
  type        = string
}

variable "route53_cname_subdomain" {
  description = "Full domain address of the CNAME that will point to the ec2 host EC2 public domain"
  type        = list(string)
}

variable "route53_cname_private_subdomain" {
  description = "Full domain address of the CNAME that will point to the ec2 host EC2 public domain"
  type        = list(string)
}

variable "route53_domain" {
  description = "Domain name to use in Route53 to add a record for ec2 host from within its own userdata"
  type        = string
}

variable "root_volume_termination" {
  description = "If True then root device volume will be destroy"
  type        = bool
  default     = true
}

variable "ebs_volumes" {
  description = "list of ebs volumes"
  type = list(object({
    device_name           = string
    volume_size           = number
    volume_type           = string
    delete_on_termination = bool
  }))
  default = null
}

variable "region" {
  description = "AWS region to deploy the ec2 in. It is used for AWS CLI calls from within ec2 host (userdata)"
  type        = string
}

variable "recovery_enabled" {
  description = "When enabled ec2 instance when failed will be rebooted"
  type        = bool
  default     = true
}

