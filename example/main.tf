provider "aws" {
  region = local.aws_region
}

data "template_file" "ec2_user_data" {
  template = file(local.ec2_user_data)

  vars = {
    HOSTNAME = local.ec2_hostname
  }
}

data "aws_ami" "ubuntu" {
  most_recent = true
  owners      = ["099720109477"]

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-bionic-18.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
}

module "vpc" {
  source = "git::https://gitlab.com/lukapo/terraform/aws/module.vpc.git?ref=2.0.3"

  aws_region      = local.aws_region
  vpc_name        = "vpc-${local.project}"
  cidr            = local.vpc_cidr
  azs             = ["${local.aws_region}a"]
  private_subnets = ["${local.vpc_cidr_prefix}.5.0/24"]
  public_subnets  = ["${local.vpc_cidr_prefix}.0.0/24"]
  tags = {
    Project     = local.project
    Environment = local.environment
  }
}

locals {
  aws_region      = "eu-central-1"
  vpc_cidr        = "10.20.0.0/16"
  vpc_cidr_prefix = "10.20"
  project         = "ec2"
  environment     = "example"
  ec2_user_data   = "user_data.sh.tpl"
  ec2_hostname    = "${local.project}-${local.environment}-public"
  ec2_key_name    = "${local.project}-${local.environment}-public"
}

resource "aws_key_pair" "this" {
  key_name   = local.ec2_key_name
  public_key = file("../ssh/test.pub")
}

resource "aws_route53_zone" "primary" {
  name = "${local.environment}.testmeonit.com"
}

module "ec2" {
  source        = "../"
  vpc_id        = module.vpc.vpc_id
  name          = local.ec2_hostname
  user_data     = data.template_file.ec2_user_data.rendered
  ami           = data.aws_ami.ubuntu.image_id
  instance_type = "t3.micro"
  subnet_ids    = module.vpc.public_subnets
  ingress_ports = [{
    port             = 22
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
    },
    {
      port             = 80
      protocol         = "tcp"
      cidr_blocks      = ["0.0.0.0/0"]
      ipv6_cidr_blocks = ["::/0"]
    },
    {
      port             = 443
      protocol         = "tcp"
      cidr_blocks      = ["0.0.0.0/0"]
      ipv6_cidr_blocks = ["::/0"]
  }]
  route53_domain                  = aws_route53_zone.primary.name
  route53_zone_id                 = aws_route53_zone.primary.id
  route53_cname_private_subdomain = ["${local.ec2_hostname}.local", "${local.ec2_hostname}.private"]
  route53_cname_subdomain         = [local.ec2_hostname]
  instance_volume_size_gb         = 10
  key_name                        = aws_key_pair.this.key_name
  ebs_volumes = [{
    device_name           = "/dev/sdi"
    volume_size           = 5
    volume_type           = "gp2"
    delete_on_termination = false
    },
    {
      device_name           = "/dev/sdj"
      volume_size           = 2
      volume_type           = "gp2"
      delete_on_termination = false
  }]
  region = local.aws_region
}



