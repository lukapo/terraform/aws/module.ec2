output "id" {
  description = "List of IDs of instances"
  value       = module.ec2.id
}

output "public_ip" {
  description = "List of public IP addresses assigned to the instances, if applicable"
  value       = module.ec2.public_ip
}

output "ipv6_addresses" {
  description = "List of assigned IPv6 addresses of instances"
  value       = module.ec2.ipv6_addresses
}

output "private_ip" {
  description = "List of private IP addresses assigned to the instances"
  value       = module.ec2.private_ip
}

output "dns_records_private" {
  value = module.ec2.dns_records_private
}

output "dns_records_public" {
  value = module.ec2.dns_records_public
}
