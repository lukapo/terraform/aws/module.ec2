#!/bin/bash
export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin

apt -y update && apt install -y python3-distro python3-pip

pip install awscli

hostname "${HOSTNAME}"
echo "${HOSTNAME}" > /etc/hostname
echo "127.0.0.1 ${HOSTNAME}" >> /etc/hosts
hostnamectl set-hostname "${HOSTNAME}"
