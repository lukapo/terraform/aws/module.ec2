resource "aws_route53_record" "ec2-records-public" {
  count   = var.associate_public_ip_address == true ? length(var.route53_cname_subdomain) : 0
  zone_id = var.route53_zone_id
  name    = element(var.route53_cname_subdomain, count.index)
  type    = "A"
  ttl     = "60"
  records = [element(module.ec2.public_ip, count.index)]
}

resource "aws_route53_record" "ec2-records-ipv6" {
  count   = var.ipv6_enable == true ? length(var.route53_cname_subdomain) : 0
  zone_id = var.route53_zone_id
  name    = element(var.route53_cname_subdomain, count.index)
  type    = "AAAA"
  ttl     = "60"
  records = [element(module.ec2.ipv6_addresses[0], count.index)]
}

resource "aws_route53_record" "ec2-records-private" {
  count   = length(var.route53_cname_private_subdomain)
  zone_id = var.route53_zone_id
  name    = element(var.route53_cname_private_subdomain, count.index)
  type    = "A"
  ttl     = "60"
  records = [element(module.ec2.private_ip, count.index)]
}

