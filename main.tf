module "ec2" {
  source  = "terraform-aws-modules/ec2-instance/aws"
  version = "~> 2.0"

  name                        = var.name
  instance_count              = var.instance_count
  user_data                   = var.user_data
  ami                         = var.ami
  instance_type               = var.instance_type
  key_name                    = var.key_name
  monitoring                  = var.monitoring
  vpc_security_group_ids      = [aws_security_group.sg-ec2.id]
  subnet_id                   = var.subnet_id
  subnet_ids                  = var.subnet_ids
  associate_public_ip_address = var.associate_public_ip_address
  ipv6_address_count          = var.ipv6_enable == true ? var.ipv6_address_count : null

  root_block_device = [
    {
      volume_type           = "gp2"
      volume_size           = var.instance_volume_size_gb
      delete_on_termination = var.root_volume_termination
    },
  ]

  tags = merge(
    {
      "Name" = var.name
    }, var.tags
  )
}

resource "aws_volume_attachment" "this_ec2" {
  count = length(var.ebs_volumes)

  device_name = var.ebs_volumes[count.index]["device_name"]
  volume_id   = aws_ebs_volume.this[count.index].id
  instance_id = module.ec2.id[0]
}

resource "aws_ebs_volume" "this" {
  count             = length(var.ebs_volumes)
  availability_zone = module.ec2.availability_zone[0]
  size              = var.ebs_volumes[count.index]["volume_size"]

  tags = {
    Name = "ebs-volume-${var.name}-${count.index}"
  }
}
